package Skardian;
import robocode.*;
import robocode.util.*;
import java.awt.Color;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

/**
 * PinkWarrior - a robot by Skardian
 */
public class PinkWarrior extends Robot
{
    double fieldX;
    double fieldY;
    boolean edgeReached;
    int dir = 1;

	/**
	 * run: PinkWarrior's default behavior
	 */
	public void run() {
		// Initialization of the robot should be put here
        fieldX = getBattleFieldWidth();
        fieldY = getBattleFieldHeight();

		// After trying out your robot, try uncommenting the import at the top,
		// and the next line:

		setColors(Color.pink, Color.pink, Color.pink); // body,gun,radar

		// Robot main loop
		while(true) {
			// Replace the next 4 lines with any behavior you would like

            while (!onEdge())
                ahead(1000);

			turnGunRight(dir * 360);
            dir *= -1;

		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		// Replace the next line with any behavior you would like
		fire(2);
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
        if (edgeReached)
        {
            back(50);
            turnLeft(90);
        }
        edgeReached = false;
	}

	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
        edgeReached = true;
	}

	/**
	 * onHitRobot: What to do when you hit a wall
	 */
	public void onHitRobot(HitRobotEvent e) {
            back(50);
            turnLeft(dir * 45);
            dir *= -1;
    }

    public boolean onEdge() {
        int range = 1;
        double x = getX();
        double y = getY();
        return edgeReached || x < range || y < range || x > (fieldX - range) || y > (fieldY - range);

    }


}
